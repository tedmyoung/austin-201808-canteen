package com.visa.ncg.canteen.api;

public class AccountCreateRequest {

  private int initialBalance;
  private String accountName;

  public int getInitialBalance() {
    return initialBalance;
  }

  public void setInitialBalance(int initialBalance) {
    this.initialBalance = initialBalance;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }
}
