package com.visa.ncg.canteen.currency;

import java.math.BigDecimal;

public interface CurrencyService {
  BigDecimal convertToGbp(int amount);
  BigDecimal convertToBtc(int amount);
}
