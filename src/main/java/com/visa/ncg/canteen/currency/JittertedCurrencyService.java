package com.visa.ncg.canteen.currency;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class JittertedCurrencyService implements CurrencyService {

  private static final String CURRENCY_CONVERSION_URL = "http://jitterted-currency-conversion.herokuapp.com/convert?from={from}&to={to}&amount={amount}";
  private final RestTemplate restTemplate = new RestTemplate();

  @Override
  public BigDecimal convertToGbp(int amount) {
    return convertToCurrency(amount, "GBP");
  }

  @Override
  public BigDecimal convertToBtc(int amount) {
    return convertToCurrency(amount, "BTC");
  }

  private BigDecimal convertToCurrency(int amount, String targetCurrency) {
    Map<String, String> params = createParameterMap(amount, targetCurrency);

    ConvertedCurrency convertedCurrency = restTemplate
        .getForObject(CURRENCY_CONVERSION_URL, ConvertedCurrency.class, params);

    return convertedCurrency.getConverted();
  }

  private Map<String, String> createParameterMap(int amount, String targetCurrency) {
    Map<String, String> params = new HashMap<>();
    params.put("from", "USD");
    params.put("to", targetCurrency);
    params.put("amount", Integer.toString(amount));
    return params;
  }

}
