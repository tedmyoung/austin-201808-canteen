package com.visa.ncg.canteen.domain;

public class Account {

  // External property
  private Long id;

  // Internal information
  private int balance = 0;
  private String name = "";

  public Account(int startingBalance) {
    balance = startingBalance;
  }

  public Account() {
  }

  public int balance() {
    return balance;
  }

  public void deposit(int amount) {
    validateAmount(amount);
    balance += amount;
  }

  private void validateAmount(int amount) {
    if (amount <= 0) {
      throw new InvalidAmountException("Amount " + amount + " was not valid.");
    }
  }

  public void withdraw(int amount) {
    validateAmount(amount);
    validateSufficientBalance(amount);
    balance -= amount;
  }

  private void validateSufficientBalance(int amount) {
    if (amount > balance) {
      throw new InsufficientBalanceException("Can't withdraw " + amount + ", balance is only " + balance);
    }
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String name() {
    return name;
  }

  public void changeNameTo(String name) {
    this.name = name;
  }
}
