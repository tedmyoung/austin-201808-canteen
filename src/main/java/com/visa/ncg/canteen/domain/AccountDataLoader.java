package com.visa.ncg.canteen.domain;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AccountDataLoader implements ApplicationRunner {
  private AccountRepository accountRepository;

  public AccountDataLoader(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    Account account0 = new Account();
    account0.deposit(10);
    account0.changeNameTo("Luxuries");
    accountRepository.save(account0); // assigned ID of 1

    Account account1 = new Account();
    account1.deposit(10);
    account1.changeNameTo("Necessities");
    accountRepository.save(account1); // assigned ID of 2

    Account account123 = new Account(10); // balance of 10
    account123.setId(123L); // force ID to be 123
    account123.changeNameTo("Savings");
    accountRepository.save(account123);
  }
}
