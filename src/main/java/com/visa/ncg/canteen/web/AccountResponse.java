package com.visa.ncg.canteen.web;

import com.visa.ncg.canteen.domain.Account;

import java.math.BigDecimal;

public class AccountResponse {
  private long id;
  private int balance;
  private String name;
  private BigDecimal gbpBalance;
  private BigDecimal bitcoinBalance;

  public AccountResponse() {
  }

  // Alternate form: static method instead of constructor
  public static AccountResponse transform(Account account) {
    return new AccountResponse(account);
  }

  // Transform domain object to a transfer object
  public AccountResponse(Account account) {
    id = account.getId();
    balance = account.balance();
    name = account.name();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setGbpBalance(BigDecimal gbpBalance) {
    this.gbpBalance = gbpBalance;
  }

  public BigDecimal getGbpBalance() {
    return gbpBalance;
  }

  public void setBitcoinBalance(BigDecimal bitcoinBalance) {
    this.bitcoinBalance = bitcoinBalance;
  }

  public BigDecimal getBitcoinBalance() {
    return bitcoinBalance;
  }
}
