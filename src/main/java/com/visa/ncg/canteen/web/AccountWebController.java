package com.visa.ncg.canteen.web;

import com.visa.ncg.canteen.currency.CurrencyService;
import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AccountWebController {

  private final Logger logger = LoggerFactory.getLogger(AccountWebController.class);
  private AccountRepository accountRepository;
  private CurrencyService currencyService;

  @Autowired
  public AccountWebController(AccountRepository accountRepository,
      CurrencyService currencyService) {
    this.accountRepository = accountRepository;
    this.currencyService = currencyService;
  }

  @GetMapping("/account")
  public String allAccountView(Model model) {
    List<Account> accounts = accountRepository.findAll();

    // The "old" way, using the for-each loop:
    //
    //    List<AccountResponse> responses = new ArrayList<>();
    //
    //    for (Account account : accounts) {
    //      AccountResponse response = new AccountResponse(account);
    //      responses.add(response);
    //    }

    //-- The Java 8 way, using Streams and Lambdas:
    List<AccountResponse> responses = accounts.stream()
                                              .map(AccountResponse::transform)
                                              .collect(Collectors.toList());
    // Add the result List<AccountResponse> to the model
    model.addAttribute("accounts", responses);

    // Can also easily sum the balance across all accounts:
    int totalBalance = accounts.stream()
                               .mapToInt(Account::balance)
                               .sum();
    // and add it to the model, too
    model.addAttribute("total", totalBalance);

    return "all-accounts";
  }


  @GetMapping("/account/{id}")
  public String accountView(@PathVariable("id") String id, Model model) {
    Long accountId = Long.parseLong(id);
    Account account = accountRepository.findOne(accountId);

    validateAccountExists(id, account);

    AccountResponse accountResponse = convertToResponse(account);

    model.addAttribute("account", accountResponse);
    return "account-view";
  }

  private void validateAccountExists(String id, Account account) {
    if (account == null) {
      logger.debug("Account not found, ID: {}", id);
      throw new NoSuchAccountException();
    }
  }

  private AccountResponse convertToResponse(Account account) {
    AccountResponse accountResponse = new AccountResponse(account);
    BigDecimal gbpBalance = currencyService.convertToGbp(account.balance());
    accountResponse.setGbpBalance(gbpBalance);
    BigDecimal bitcoinBalance = currencyService.convertToBtc(account.balance());
    accountResponse.setBitcoinBalance(bitcoinBalance);
    return accountResponse;
  }

  @ExceptionHandler({NoSuchAccountException.class})
  public ModelAndView accountNotFoundHandler() {
    // create a model-and-view to hold status and view name
    ModelAndView mav = new ModelAndView();
    // set the status code to be not found (404)
    mav.setStatus(HttpStatus.NOT_FOUND);
    // return the name of the error view to handle this case
    mav.setViewName("account-not-found");

    return mav;
  }

  @GetMapping("/create-account")
  public String createAccountForm(Model model) {
    // put a default account name into the form
    model.addAttribute("accountName", "default");
    return "create-account";
  }

  @PostMapping("/create-account")
  public String createAccount(@ModelAttribute("accountName") String name) {
    Account account = new Account();
    account.changeNameTo(name);
    account = accountRepository.save(account);
    return "redirect:/account/" + account.getId();
  }
}
