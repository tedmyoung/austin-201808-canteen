package com.visa.ncg.canteen;

import com.visa.ncg.canteen.api.AccountApiController;
import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import com.visa.ncg.canteen.web.AccountResponse;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountApiControllerTest {

  @Test
  public void testGetMapping() throws Exception {
    AccountRepository accountRepository = new AccountRepository();
    Account account = new Account(10);
    account.setId(123L);
    accountRepository.save(account);
    AccountApiController controller = new AccountApiController(accountRepository);
    AccountResponse accountResponse = controller.accountInfo("123");
    assertThat(accountResponse.getId())
        .isEqualTo(123L);
  }

}