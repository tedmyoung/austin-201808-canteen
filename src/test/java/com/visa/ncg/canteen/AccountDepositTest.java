package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.InvalidAmountException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountDepositTest {

  @Test
  public void negativeDepositAmountThrowsException() {
    Account account = new Account();

    // expect that an exception was thrown
    assertThatThrownBy(() -> {
      account.deposit(-10);
    })
    .isInstanceOf(InvalidAmountException.class)
    .hasMessage("Amount -10 was not valid.");
  }

  @Test
  public void zeroDepositAmountThrowsException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(0);
    })
    .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void newAccountHasZeroBalance() throws Exception {
    Account account = new Account();

    assertThat(account.balance())
        .isZero();
  }

  @Test
  public void deposit10DollarsResultsInBalanceOf10Dollars() throws Exception {
    Account account = new Account();

    account.deposit(10);

    assertThat(account.balance())
        .isEqualTo(10);
  }

  @Test
  public void deposit7ToAccountWith5ResultsIn12() throws Exception {
    Account account = new Account(5);

    account.deposit(7);

    assertThat(account.balance())
        .isEqualTo(12);
  }

  @Test
  public void deposit6Then8ResultsIn14() throws Exception {
    Account account = new Account();

    account.deposit(6);
    account.deposit(8);

    assertThat(account.balance())
        .isEqualTo(14);
  }
}