package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.InsufficientBalanceException;
import com.visa.ncg.canteen.domain.InvalidAmountException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountWithdrawTest {
  @Test
  public void withdraw3DollarsFromAccountHaving7DollarsResultsIn4DollarBalance() throws Exception {
    Account account = new Account(7);

    account.withdraw(3);

    assertThat(account.balance())
        .isEqualTo(4);
  }

  @Test
  public void withdraw5Then3ResultsIn12DollarBalance() throws Exception {
    Account account = new Account(20);

    account.withdraw(5);
    account.withdraw(3);

    assertThat(account.balance())
        .isEqualTo(12);
  }

  @Test
  public void withdraw0ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdrawNegativeAmountThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(-7);
    })
        .isInstanceOf(InvalidAmountException.class)
        .hasMessage("Amount -7 was not valid.");
  }

  @Test
  public void withdrawMoreThanBalanceThrowsInsufficientBalanceException() throws Exception {
    Account account = new Account(3);

    assertThatThrownBy(() -> {
      account.withdraw(4);
    })
        .isInstanceOf(InsufficientBalanceException.class)
        .hasMessage("Can't withdraw 4, balance is only 3");
  }

  @Test
  public void canWithdrawAllMoney() throws Exception {
    Account account = new Account(8);

    account.withdraw(8);

    assertThat(account.balance())
        .isZero();
  }

}