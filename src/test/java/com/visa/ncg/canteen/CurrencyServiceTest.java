package com.visa.ncg.canteen;

import com.visa.ncg.canteen.currency.CurrencyService;
import com.visa.ncg.canteen.currency.JittertedCurrencyService;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class CurrencyServiceTest {

  @Test
  public void usingMockConvertUsdToGbpResultsIn10() {
    CurrencyService currencyService = mock(JittertedCurrencyService.class);
    when(currencyService.convertToGbp(anyInt()))
        .thenReturn(BigDecimal.TEN);

    assertThat(currencyService.convertToGbp(99))
        .isEqualTo(BigDecimal.TEN);
  }

  @Test
  public void usingFakeConvertToGbpResultsIn99() throws Exception {
    CurrencyService currencyService = new FakeCurrencyService();

    assertThat(currencyService.convertToGbp(1000))
        .isEqualTo(BigDecimal.valueOf(99));
  }

}

