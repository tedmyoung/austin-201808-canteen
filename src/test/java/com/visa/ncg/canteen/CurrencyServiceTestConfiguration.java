package com.visa.ncg.canteen;

import com.visa.ncg.canteen.currency.CurrencyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * In our tests, we want to use the "Fake" Currency Service so that we don't
 * rely on an external system already running.
 *
 * By creating this configuration file, annotated with @Configuration,
 * and placing it in the test directory, Spring will look here for additional
 * configuration when running the tests.
 *
 * Any methods annotated with @Bean means that Spring will call the method when
 * it needs an instance matching the return value. See below.
 */
@Configuration
class CurrencyServiceTestConfiguration {
  @Bean    // This tells Spring to look here when searching for a CurrencyService component
  @Primary // Tell Spring to use this one, instead of the JittertedCurrencyService
  public CurrencyService fakeCurrencyService() {
    return new FakeCurrencyService();
  }
}
