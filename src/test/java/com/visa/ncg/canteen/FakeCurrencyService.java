package com.visa.ncg.canteen;

import com.visa.ncg.canteen.currency.CurrencyService;

import java.math.BigDecimal;

class FakeCurrencyService implements CurrencyService {
  @Override
  public BigDecimal convertToGbp(int amount) {
    return BigDecimal.valueOf(99);
  }

  @Override
  public BigDecimal convertToBtc(int amount) {
    return BigDecimal.ONE;
  }
}
